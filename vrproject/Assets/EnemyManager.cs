﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyManager : MonoBehaviour
{
    public GameObject enemy;
    //public GameObject enemySpawn;
    public float time;
    public float counter;
    public GameObject[] spawnpoints;
    int rand;
    public int life;

    public
    // Use this for initialization
    void Start()
    {
        
        time = Random.Range(1.5f, 3f);


    }

    // Update is called once per frame
    void Update()
    {
        
        Spawntimer();
        

    }
    void Spawntimer()
    {
        
        counter += Time.deltaTime;
        if (counter >= time)
        {
            rand = Random.Range(0,3);
            enemy = Instantiate(enemy, spawnpoints[rand].transform.position, transform.rotation);
            time = Random.Range(1.5f, 3f);
            counter = 0;
        }

    }

    public void takeDmg()
    {
        life--;
#if UNITY_EDITOR
        if (life <= 0)
        {
            SceneManager.LoadScene("vrprojecttestlevel");
        }
#else
        if (life <= 0)
        {
            SceneManager.LoadScene("vrprojecttestlevel");
        }
#endif
    }


}

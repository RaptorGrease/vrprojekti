﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    public GameObject goal;
    NavMeshAgent agent;
    public GameObject enemym;
    int eLife = 3;


    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        goal = GameObject.FindGameObjectWithTag("Goal");
        agent.destination = goal.transform.position;
        enemym = GameObject.Find("EnemyManager");

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Goal")
        {
            enemym.GetComponent<EnemyManager>().takeDmg();
            Destroy(gameObject);
            
        }
        if (other.gameObject.tag == "Bullet")
        {
            eLife--;
            if (eLife <= 0)
            {
                Destroy(gameObject);
            }

        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMngr : MonoBehaviour {

    public GameObject menupanel;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void Awake()
    {
        Time.timeScale = 0;
    }

    public void Startgame()
    {
        Time.timeScale = 1;
        menupanel.SetActive(false); 
    }

    public void Quitgame()
    {
#if UNITY_EDITOR

         UnityEditor.EditorApplication.isPlaying = false;

#else
       
          Application.Quit();
        
#endif
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunUI : MonoBehaviour {

    public Image ammoBar;
    public Image coolDownTimer;

    private void Awake()
    {
        coolDownTimer.fillAmount = 0f;
        ammoBar.fillAmount = 1f;
    }

    public void SetAmmoFill(float fill)
    {
        fill = Mathf.Clamp(fill, 0f, 1f);
        ammoBar.fillAmount = fill;
    }

    public void SetCDTimer(float fill)
    {
        fill = Mathf.Clamp(fill, 0f, 1f);
        coolDownTimer.fillAmount = fill;
    }
}

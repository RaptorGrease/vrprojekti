﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {

    public float initialSpeed = 25f;
    public float lifeTime = 3f;

    private float speed;
    private Vector3 direction;
    private Rigidbody rb;

	void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
	
	void FixedUpdate() {
        rb.MovePosition(rb.position + (speed * direction * Time.deltaTime));
	}

    public void Launch(Vector3 dir)
    {
        direction = dir;
        speed = initialSpeed;
        StartCoroutine(destroyTimer(lifeTime));
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.CompareTag("Wall")) Destroy(gameObject);
        if (col.collider.CompareTag("Cube"))
        {
            col.gameObject.GetComponent<Rigidbody>().AddForce(0, 15f, 15f, ForceMode.Impulse);
            Destroy(gameObject);
        }
    }

    IEnumerator destroyTimer(float t)
    {
        yield return new WaitForSeconds(t);
        Destroy(gameObject);
    }
}
